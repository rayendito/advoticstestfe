import './DefaultLayout.scss'
import Navbar from './../components/Navbar'
import Sidebar from '../components/Sidebar';

function DefaultLayout({ children }){
    return (
        <div>
            <Navbar />
            <div className="navigationWrapper">
                <div>
                    <Sidebar />
                </div>
                <div style={{flexGrow: 1}}>
                    { children }
                </div>
            </div>
        </div>
    )
}

export default DefaultLayout;