import { useState } from 'react';

import './page_styles/Dashboard.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronUp } from '@fortawesome/free-solid-svg-icons'
import More from './../assets/more.svg';
import DownArrow from './../assets/DownArrow.svg';
import SaleTurn from './../assets/Sales Turnover.svg';

import Rank from '../components/Rank';
import DatePicker from '../components/DatePicker';

import {
    HorizontalGridLines,
    XAxis,
    FlexibleXYPlot,
    YAxis,
    LineMarkSeries,
    VerticalBarSeries,
    Crosshair
} from 'react-vis';

// time things
const MSEC_DAILY = 86400000;
const datethen = new Date().setDate(new Date().getDate() - 7)

// mockup data from random
function randomizeData(min, max){
    var toret = []
    for(var i = 0; i <= 6; i++){
        var x = datethen + MSEC_DAILY *  i;
        var y = Math.floor(Math.random() * (max - min + 1) + min)
        toret.push({
            x: x,
            y: y
        })
    }

    return toret
}

// static data for visualization
const grossData = randomizeData(10000, 20000);

const nettData = randomizeData(500, 1000);

const APVData = randomizeData(10000, 25000);

const DATA = [
    grossData,
    nettData,
    APVData
]

function Legend(props){
    return (
        <div className='legendContainer'>
            <div className='legendColor' style={{background : props.color}}></div>
            <p>{props.text}</p>
        </div>
    )
}

function Dashboard(){
    const [ crosshairValues, setCrosshair ] = useState({
        crosshairValues : []
    })

    const onMouseLeave = () => {
        setCrosshair({crosshairValues: []});
    };

    const onNearestX = (value, {index}) => {
        setCrosshair({crosshairValues: DATA.map(d => d[index])});
    };

    return (
        <div className="dashboardContainer">
            <div className="titleAndCalThumbnail">
                <h1 className='title'>Dashboard</h1>
                <DatePicker />
            </div>

            <div className="marketInsightBar">
                <h2 id='marketInsightText'>MARKET INSIGHTS</h2>
                <div className="clickForHelp">
                    <img className='helplogo' src={require('./../assets/help.png')} alt="helplogo" />
                    <p><u>Click Here for Help</u></p>
                    <FontAwesomeIcon icon={faChevronUp} className="helplogo" />
                </div>
            </div>

            <div className="salesTurnover whiteBoxAndShadow">
                <div className='sectionHeader'>
                    <p>Sales Turnover</p>
                    <img src={More} alt="MoreLogo" className='morelogo' />
                </div>

                <div className="salesSect">
                    <div className="number">
                        <h2>Rp 3,600,000</h2>
                        <p>
                            <img src={DownArrow} alt="MoreLogo" /> <b>13.8%</b> last period products sold
                        </p>
                    </div>

                    <div className="cartIcon">
                        <img src={SaleTurn} alt="SaleTurn" />
                    </div>
                </div>

            </div>

            <div className="bottomContainer">
                <div className="avgPurchaseVal whiteBoxAndShadow">
                    <div className="sectionHeader">
                        <p>AVERAGE PURCHASE VALUE</p>
                        <img src={More} alt="MoreLogo" className='morelogo' />
                    </div>
                    <FlexibleXYPlot
                        xType="time"
                        stackBy="y"
                        margin={{bottom: 40, left: 50, right: 10, top: 10}}
                        onMouseLeave={onMouseLeave}
                        height = {400}
                    >
                        <XAxis />
                        <YAxis />
                        <HorizontalGridLines/>
                        <VerticalBarSeries
                            data={DATA[0]}
                            color={'#37B04C'}
                        />
                        <VerticalBarSeries
                            data={DATA[1]}
                            color={'#289E45'}
                        />
                        <LineMarkSeries
                            onNearestX={onNearestX}
                            data={DATA[2]}
                            color={'#FFE854'}
                        />
                        <Crosshair
                            values={crosshairValues.crosshairValues}
                            titleFormat={(d) => ({title: 'Date', value: new Date(d[0].x).toLocaleDateString()})}
                            itemsFormat={(d) => [
                                {title: 'Gross', value: d[0].y},
                                {title: 'Nett', value: d[1].y},
                                {title: 'APV', value: d[2].y}
                            ]}
                        />
                    </FlexibleXYPlot>
                    <div className="legends">
                        <Legend text="Gross" color="#37B04C" />
                        <Legend text="Nett" color="#289E45" />
                        <Legend text="APV" color="#FFE854" />
                    </div>
                </div>

                <Rank
                    rank_title="BEST SELLING SKU"
                    prodImgLinkTop = "https://img.freepik.com/free-psd/cosmetic-product-packaging-mockup_1150-40282.jpg?w=2000"
                    prodImgLink = "https://upload.wikimedia.org/wikipedia/commons/6/65/Product_Photography.jpg"
                    prodName = "Example Ranks One"
                    prodPrice = {50000}
                    prodSold = {18}
                />

                <Rank
                    rank_title="TOP COMPETITOR SKU"
                    prodImgLinkTop = "https://cdn.shopify.com/s/files/1/2303/2711/files/2_e822dae0-14df-4cb8-b145-ea4dc0966b34.jpg?v=1617059123"
                    prodImgLink = "https://www.junglescout.com/wp-content/uploads/2021/01/product-photo-water-bottle-hero.png"
                    prodName = "Example Ranks Two"
                    prodPrice = {30000}
                    prodSold = {22}
                />
            </div>
        </div>
    )
}

export default Dashboard;