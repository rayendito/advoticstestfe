import './App.css';
import DefaultLayout from './layout/DefaultLayout';
import Dashboard from './pages/Dashboard';
import './components/comp_styles/Calendar.scss';

function App() {
  return (
    <div className='App'>
      <DefaultLayout>
        <Dashboard />
      </DefaultLayout>
    </div>
  );
}

export default App;
