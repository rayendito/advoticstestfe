import './comp_styles/Sidebar.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBarsStaggered } from '@fortawesome/free-solid-svg-icons'
import  AnalyticsLogo from './../assets/Dashboard icon.svg';

function Sidebar(){
    return(
        <div className="sidebarContainer">
            <FontAwesomeIcon icon={faBarsStaggered} className="staggeredMenuIcon" />
            <div className="sidebarTab">
                <img src={AnalyticsLogo} alt="AnalyticsLogo" className='analyticsIcon' />
            </div>
        </div>
    )
}

export default Sidebar;