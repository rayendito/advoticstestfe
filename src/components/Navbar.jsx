import './comp_styles/Navbar.scss'
import Profile from './../assets/Profile.svg';

function Navbar(){
    return (
        <>
            <div className="NavbarContainer">
                <div className="mainlogo">
                    <img className='biglogo' src={require('./../assets/advotics_logo.png')} alt="biglogo" />
                    <p className='poweredby'>powered by</p>
                    <img className='smalllogo' src={require('./../assets/advotics_logo.png')} alt="smalllogo" />
                </div>

                <div className="usercontainer">
                    <div className="names">
                        <p id="username" >Username</p>
                        <p id="compname">Company Name</p>
                    </div>
                    <img src={Profile} alt="ProfileIcon" className='profilelogo' />
                    <img className='logoutlogo' src={require('./../assets/logout.png')} alt="logo" />
                </div>
            </div>
        </>
    )
}

export default Navbar;