import './comp_styles/Rank.scss'
import More from './../assets/more.svg';

/**
 * 
 * @param {*} props - isTopRank, prodImgLink, prodName, prodPrice, prodSold
 * @returns 
 */

function RankEntry(props) {
    return(
        <div className={props.isTopRank ? 'rankEntryTop' : 'rankEntry'}>
            <img src={props.prodImgLink} alt="top-prod" className={props.isTopRank ? 'imgRankTop' : 'imgRank'}/>
            <div className="prodDetails">
                <p>{props.prodName}</p>
                <div className="prodStat">
                    <p>Rp {props.prodPrice}</p>
                    <p>{props.prodSold}</p>
                </div>
            </div>
        </div>
    )
}

function Rank(props){
    var items = []

    for (var i=0; i < 4; i++) {
        items.push(
            <RankEntry
                key = {i}
                isTopRank = {false}
                prodImgLink = {props.prodImgLink}
                prodName = {props.prodName}
                prodPrice = {props.prodPrice}
                prodSold = {props.prodSold}
            />
        )
    }


    return(
        <div className='whiteBoxAndShadow'>
            <div className="sectionHeader">
                <p>{ props.rank_title }</p>
                <img src={More} alt="MoreLogo" className='morelogo' />
            </div>
            <div className="ranksContainer">
                <RankEntry 
                    isTopRank = {true}
                    prodImgLink = {props.prodImgLinkTop}
                    prodName = {props.prodName}
                    prodPrice = {props.prodPrice}
                    prodSold = {props.prodSold}
                />
                {items}
            </div>
        </div>
    )
}

export default Rank;