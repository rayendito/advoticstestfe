import { useState } from 'react';
import './comp_styles/DatePicker.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown, faXmark } from '@fortawesome/free-solid-svg-icons'
import Calendar from 'react-calendar';

function DatePicker(){
    const [date, setDate] = useState(
        [
            getDateXDaysAgo(7),
            getDateXDaysAgo(1)
        ]
    )
    const [savedDate, setSavedDate] = useState(date)

    function removeCalOptionClasses(){
        var elements = document.getElementsByClassName('borderBottom');
        for (var i = 0; i < elements.length; i++) {
            elements.item(i).classList.remove("buttonHighlight");
        }
        document.getElementById('custom').classList.remove("buttonHighlight");
    }

    function togglePicker(){
        if(document.getElementById('dpicker').style.display === 'none'){
            document.getElementById('dpicker').style.display = 'block';
            setSavedDate(date);
        }
        else{
            document.getElementById('dpicker').style.display = 'none';
            setDate(savedDate);
            removeCalOptionClasses();
        }
    }

    document.addEventListener('click', function handleClickOutsideBox(event) {
        const box = document.getElementById('someid');
        
        if (!box.contains(event.target)) {
            document.getElementById('dpicker').style.display = 'none';
            setDate(savedDate);
            removeCalOptionClasses();
        }
    });

    function saveDate(){
        setSavedDate(date);
        document.getElementById('dpicker').style.display = 'none';
    }

    // calendar selection functions
    function getDateXDaysAgo(numOfDays, date = new Date()) {
        const daysAgo = new Date(date.getTime());
        daysAgo.setDate(date.getDate() - numOfDays);
        return daysAgo;
    }

    function getDateXMonthsAgo(months, date = new Date()) {
        date.setMonth(date.getMonth() + months);
        return date;
    }

    function selectCalendar(selection) {
        // remove highlight class
        removeCalOptionClasses();

        if(selection === 'today' || selection === 'custom'){
            setDate(
                [
                    new Date(),
                    new Date()
                ]
            )
        }
        else if(selection === 'yesterday'){
            setDate(
                [
                    getDateXDaysAgo(1),
                    getDateXDaysAgo(1)
                ]
            )
        }
        else if(selection === 'sevendays'){
            setDate(
                [
                    getDateXDaysAgo(7),
                    getDateXDaysAgo(1)
                ]
            )
        }
        else if(selection === 'thirtydays'){
            setDate(
                [
                    getDateXDaysAgo(30),
                    getDateXDaysAgo(1)
                ]
            )
        }
        else if(selection === 'thismonth'){
            var date = new Date(), y = date.getFullYear(), m = date.getMonth();
            setDate(
                [
                    new Date(y, m, 1),
                    new Date(y, m + 1, 0)
                ]
            )
        }
        document.getElementById(selection).classList.add("buttonHighlight");
    }

    return(
        <div id="someid" className="datePickerContainer">
            <div className="datePickerThumbnail whiteBoxAndShadow" onClick={togglePicker}>
                <img className='calendarlogo' src={require('./../assets/calendar.png')} alt="calendarlogo" />
                <p style={{"color": "gray"}}>Period</p>
                <p>{date[0].toDateString()} - {date[1].toDateString()}</p>
                <FontAwesomeIcon icon={faChevronDown} />
            </div>
            
            <div id="dpicker" className="datePicker">
                <div className="dateHeader">
                    <div className='dateTitle'>
                        <img className='calendarlogo' src={require('./../assets/calendar.png')} alt="calendarlogo" />
                        <p>Period</p>
                    </div>
                    <FontAwesomeIcon icon={faXmark} onClick={togglePicker} />
                </div>
                <div className="optionsAndCalendar">
                    <div className="options">
                        <p id='today' className='borderBottom' onClick={() => selectCalendar('today')}><nobr>Today</nobr></p>
                        <p id='yesterday' className='borderBottom' onClick={() => selectCalendar('yesterday')}><nobr>Yesterday</nobr></p>
                        <p id='sevendays' className='borderBottom buttonHighlight' onClick={() => selectCalendar('sevendays')}><nobr>Last 7 days</nobr></p>
                        <p id='thirtydays' className='borderBottom' onClick={() => selectCalendar('thirtydays')}><nobr>Last 30 days</nobr></p>
                        <p id='thismonth' className='borderBottom' onClick={() => selectCalendar('thismonth')}><nobr>This month</nobr></p>
                        <p id='custom' onClick={() => selectCalendar('custom')}><nobr>Custom</nobr></p>
                        <button className='applyButton' onClick={saveDate}>Apply</button>
                    </div>
                    <div className="sepline"></div>
                    <Calendar
                        onChange={setDate}
                        value={date}
                        selectRange={true}
                        next2Label={null}
                        prev2Label={null}
                        maxDate={getDateXDaysAgo(1)}// yesterday
                        minDate={getDateXMonthsAgo(-6)} // 6 mo ago
                    />
                </div>
            </div>
        </div>
    )
}

export default DatePicker;